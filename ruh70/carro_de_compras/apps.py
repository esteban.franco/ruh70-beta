from django.apps import AppConfig


class CarroDeComprasConfig(AppConfig):
    name = 'carro_de_compras'

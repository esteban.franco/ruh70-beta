from django.shortcuts import render
from .carro import Carro
from tienda.models import Producto
from django.shortcuts import redirect # cada vez que ejecutemos acciones en el carro debemos redireccionar estas acciones
                                      # a la vista del carro

# Create your views here.
def agregar_producto(request, producto_id):
      carro =Carro(request)  # llamando al carro o creando uno nuevo
      producto = Producto.objects.get(id=producto_id)  # obtener el producto a agregar al carro

      carro.agregar_producto(producto=producto)

      return redirect("tienda")  

def eliminar_producto(request, producto_id):
      carro =Carro(request)  # llamando al carro 
      producto = Producto.objects.get(id=producto_id)  # obtener el producto a eliminar

      carro.eliminar(producto=producto)

      return redirect("tienda") 

def restar_producto(request, producto_id):
      carro =Carro(request)  # llamando al carro o creando uno nuevo
      producto = Producto.objects.get(id=producto_id)  # obtener el producto a agregar al carro

      carro.restar_producto(producto=producto)

      return redirect("tienda") 

def limpiar_carro(request):
      carro =Carro(request)  # llamando al carro o creando uno nuevo
      
      carro.limpiar_carro()

      return redirect("tienda") 

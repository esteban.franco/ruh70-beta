from tienda.models import Producto

class Carro:

      def __init__(self, request):
            self.request=request    # almacenar la peticion para ser desarrollada en otra parte
            self.session = request.session

            # construyendo un carro de compra para la sesion del usuario de turno
            carro = self.session.get("carro")
            
            if not carro:
                  carro = self.session["carro"] ={}
                            
            self.carro = carro
      
      def guardar_carro(self):
            """para guardar la sesion del carrito"""
            self.session["carro"] = self.carro
            self.session.modified = True
      
      def agregar_producto(self,producto):
            """ un metodo paga agregar productos al carro""" 
            if str(producto.id) not in self.carro.keys():
                  self.carro[producto.id]={
                        "producto_id":producto.id,
                        "nombre":producto.nombre,
                        "precio":producto.precio,
                        "cantidad":1,
                        "imagen":'producto.image.url'
                  } 
                  # al agregar un producto se reduce el stock de ese producto         
                  producto.reduce_stock(1)
            else:
                  # for key, value in self.carro.items():
                  #       if key ==str(producto.id):
                  #             value["cantidad"] = value["cantidad"] + 1
                  #             break
                  
                  self.carro[str(producto.id)]["cantidad"] += 1
                  self.carro[str(producto.id)]["precio"] += float(producto.precio)
                  # al agregar un producto se reduce el stock de ese producto
                  producto.reduce_stock(1)
            self.guardar_carro()
      
      def eliminar(self,producto):
            if str(producto.id) in self.carro:
                  producto.increase_stock(self.carro[str(producto.id)]["cantidad"])
                  del self.carro[str(producto.id)]
                  self.guardar_carro()

      def restar_producto(self,producto):
            """restando un producto del carrito"""
            # for key, value in self.carro.items():
            #       if key ==str(producto.id):
            #             value["cantidad"] = value["cantidad"] - 1
            #             if value["cantidad"] < 1:
            #                   self.eliminar(producto)
            #             break 
            self.carro[str(producto.id)]["cantidad"] -= 1
            self.carro[str(producto.id)]["precio"] -= float(producto.precio)
            if self.carro[str(producto.id)]["cantidad"] < 1:
                  self.eliminar(producto)
                  
            producto.increase_stock(1)
            self.guardar_carro()

      def limpiar_carro(self):
            """Para empezar nuevamente el carro desde cero"""
            # devolviendo todos los productos al stock de la bodega
            for key, value in self.carro.items():
                  p = Producto.objects.get(id=key)
                  p.increase_stock(self.carro[key]["cantidad"])

            self.session["carro"]={}
            self.session.modified=True


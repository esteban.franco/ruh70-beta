
def importe_total_carro(request):
      """funcion donde se define el procesador de contexto, para poder crear las variables globales"""
      total=0   # variable global donde quiero sumar lo que hay en el carro
      if request.user.is_authenticated:
            if not "carro" in request.session:
                  total = 0.0
            else:
                  for key, value in request.session["carro"].items():
                        total += float(value["precio"])
      
      # ----------------------ESTES ES CODIGO PROPIO -----------------------
      else: # a pesar de que no haya un usuario autenticado que sume los precios en la lista de compras.
            if "carro" in request.session:
                  for key, value in request.session["carro"].items():
                        total += float(value["precio"])
            else:
                 total = 0 
      # ----------------------ESTE ES CODIGO PROPIO -----------------------
      return {"importe_total_carro": total}



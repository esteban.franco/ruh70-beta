from carro_de_compras import carro
from django.urls import path
from . import views


app_name= "carro" # asignacion de un nickname para evitar rutas o colisiones entre links que van hacia la misma direccion
                  # asi cuando colocamos en la dirección de una url carro:;agregar sabemos que nos estamos refiriendo a estas urls

urlpatterns = [

path('agregar/<int:producto_id>/',views.agregar_producto, name='agregar_prod'),
path('eliminar/<int:producto_id>/',views.eliminar_producto, name='eliminar_prod'),
path('restar/<int:producto_id>/',views.restar_producto, name='restar_prod'),
path('limpiar/',views.limpiar_carro, name='limpiar'),

]


from django.contrib import admin

from .models import CategoriaProducto, Producto

# Register your models here.

class CategoriaProductoAdmin(admin.ModelAdmin):

      readonly_fields=("created","updated" )

class ProductoAdmin(admin.ModelAdmin):

      readonly_fields=("created","updated" )
      # que se debe desplegar en el panel de administracion
      list_display=('nombre', 'precio', 'categorias','stock')
      # porque campos es importante buscar en dicho panel.
      search_fields=('nombre','categorias')

admin.site.register(CategoriaProducto,CategoriaProductoAdmin)
admin.site.register(Producto,ProductoAdmin)

from django.db import models


# Create your models here.

class CategoriaProducto(models.Model):
      nombre=models.CharField(max_length=50, verbose_name='Categoria producto')
      created=models.DateTimeField(auto_now_add=True)
      updated=models.DateTimeField(auto_now=True)

      class Meta:
            verbose_name='Categoria producto'
            verbose_name_plural='Categorias producto'

      def __str__(self):
            return self.nombre

class Producto(models.Model):
      nombre=models.CharField(max_length=50, verbose_name='Nombre producto', default='producto defecto')
      categorias = models.ForeignKey(CategoriaProducto, on_delete=models.CASCADE)
      image=models.ImageField(upload_to='tienda',null=True,blank=True)
      precio = models.IntegerField(default=0)
      disponibilidad= models.BooleanField(default=True)
      stock = models.IntegerField(default=1, verbose_name="disponible bodega")
      created=models.DateTimeField(auto_now_add=True)
      updated=models.DateTimeField(auto_now=True)

      class Meta:
            verbose_name='Producto'
            verbose_name_plural='Productos'

      def __str__(self):
            return self.nombre

      def increase_stock(self, numero):
            self.stock += numero
            self.save()

      def reduce_stock(self,numero):
            self.stock -= numero
            self.save()
            

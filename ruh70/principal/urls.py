#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 11:11:12 2021

@author: esteban
"""

from django.urls import path
from principal import views
from django.conf import settings  # para poder importar variables de settings.py
from django.conf.urls.static import static # para agregar caminos estaticos del servidor como URLs de la aplicación

urlpatterns = [
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
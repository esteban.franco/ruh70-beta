from django.shortcuts import render, redirect
from .forms import FormularioContacto
from django.core.mail import send_mail, EmailMessage
from ruh70 import settings
# Create your views here.

def contactanos(request):
    formulario_contacto = FormularioContacto()

    if request.method == "POST":
        
        formulario_contacto = FormularioContacto( data=request.POST) # cargando en nuestro formulario la informacion
                                                                    # que el usuario introdujo.
        if formulario_contacto.is_valid():
            info_form = formulario_contacto.cleaned_data
            nombre = info_form['nombre']
            email = info_form['email']
            asunto = info_form['asunto']
            contenido = request.POST.get('contenido') # solo para hacerlo de manera diferente 
                                                      # se puede hacer con el metodo cleaned_data o con el reques.POST.get
            email_from = settings.EMAIL_HOST_USER
            
            # -- Hay 2 formas de enviar el correo, usando send_email o EmailMessage ------ #
            #  message  = f'Email enviado por {nombre} \n {email} \n \n {contenido}'                                       
            #  send_mail(subject=f'{nombre}, {asunto}', message=message, from_email=email_from, settings.EMAIL_HOST_USER)
            first_name = nombre.split(' ')[0]
            
            # enviando email a usuario por medio de EmailMessage
            message_user = (f'Hola {first_name}, \n {email} \n \n hemos recibido tu correo, '
                            f'estaremos en contacto contigo pronto. \n \n {contenido}')
            
            email_structure=EmailMessage(
                subject='[respuesta automática] hemos recibido tu solicitud',
                body=message_user,
                from_email=email_from,
                to=[email],
                bcc=[email_from]
                )
                # send_mail(subject=f'{nombre}, {asunto}', message=message, from_email=email_from, 
                #             recipient_list=settings.EMAIL_HOST_USER)

            try:
                email_structure.send()
                return redirect("/contactanos/?valido") # redireccionando a la misma pagina pero cuando se ha enviado la información válida.
            except Exception as e:
                  
                return redirect("/contactanos/?novalido")

    return render(request,'contactanos.html',{'miFormulario':formulario_contacto})



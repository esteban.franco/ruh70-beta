from django import forms

class FormularioContacto(forms.Form):
    nombre = forms.CharField(label='Dinos tu nombre', max_length=100, required=True)
    asunto = forms.CharField(label='Asunto',max_length=150,required=False)
    contenido = forms.CharField(label='Cuentanos como ayudarte', widget=forms.Textarea)
    email = forms.EmailField(label='Danos tu email',required=True)


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 11:11:12 2021

@author: esteban
"""

from django.urls import path
from . import views

urlpatterns = [
    path('', views.inversiones, name='inversiones'),
]

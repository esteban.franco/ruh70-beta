from django.shortcuts import render

# Create your views here.

from inversiones.models import  Servicio

def inversiones(request):

    Inversiones = Servicio.objects.all() # importando todos los servicios existentes de la clase servicio.

    return render(request,'inversiones.html',{"Inversiones":Inversiones})
from django.db import models

# Create your models here.
class InvestmentStages(models.Model):

      title = models.CharField(verbose_name='nombre estado', max_length=40)
      descripcion = models.TextField(verbose_name='Descripcion estado')

      
      # gathering = models.BooleanField(verbose_name='Recolección monetaria')
      # withdrawal = models.BooleanField(verbose_name='Desembolso a proyecto')
      # deployment = models.BooleanField(verbose_name='Desarrollo')
      # product_delivery = models.BooleanField(verbose_name='Entrega producto empresa asociada')
      # checking = models.BooleanField(verbose_name='Chequeo de produccion')
      # pago = models.BooleanField(verbose_name='Pago a proyecto asociado')
      # investment_return = models.BooleanField(verbose_name='retorno de la inversion')
      # profit = models.BooleanField(verbose_name='Ganancia')

      def __str__(self):
            return self.title

class Servicio(models.Model):
      title = models.CharField(verbose_name='Titulo', max_length=60)
      description = models.TextField(verbose_name='Descripción')
      investment_time = models.IntegerField(verbose_name='Tiempo de inversion (meses)')
      income = models.FloatField(verbose_name='% Retorno')
      industry = models.CharField(verbose_name='Industria', max_length=20)
      contract_type = models.CharField(max_length=30,verbose_name='Tipo de contrato')
      contract = models.FileField(verbose_name='Contrato', upload_to='Oportunidades/contratos')
      invest_stages = models.ManyToManyField(InvestmentStages, verbose_name='Etapas Inversion')
      image = models.ImageField(verbose_name='Imagen o simbolo',upload_to='Oportunidades/img')
      min_amount = models.IntegerField()
      created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha creación')
      updated = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de actualización')

      # esta clase controlará todas las opciones de visibilidad que queremos para nuestro modelo {Servicios} dentor
      # de la plataforma. En este caso configuraremos como será su nombre en singular y plural dentro de la plataforma
      # debemos entonces ver este modelo como {Oportunidad / Oportunidades} y no como 'servicios'.
      class Meta:
            verbose_name = 'Oportunidad'
            verbose_name_plural = 'Oportunidades'
            ordering = ['updated']

      def __str__(self):
            return self.title
      
# General admin settings


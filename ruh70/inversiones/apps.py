from django.apps import AppConfig


class InversionesConfig(AppConfig):
    name = 'inversiones'
    #label = 'inversiones_label'
from django.contrib import admin
from django.db.models.fields import DateField
from inversiones.models import Servicio, InvestmentStages

class servicioAdmin(admin.ModelAdmin):
      # que se debe desplegar en el panel de administracion
      list_display=('title', 'investment_time', 'industry','min_amount','income')
      # porque campos es importante buscar en dicho panel.
      search_fields=('title','industry')
      # Por cuales campos es importante generar filtros.
      list_filter=('title','updated')
      date_hierarchy='created'
      # campos que se veran en la creación del objeto pero solo como lectura
      readonly_fields=('created','updated')

class InvestmentStagesAdmin(admin.ModelAdmin):
      list_display=('title',)
      
# Register your models here.
admin.site.register(Servicio, servicioAdmin)
admin.site.register(InvestmentStages, InvestmentStagesAdmin)
#admin.site.register(InvestmentStages)
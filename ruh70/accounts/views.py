from django.shortcuts import render

# Create your views here.

def login(request):
      ctx =  {next:request.path}
      return render(request,'registration/login.html',ctx)

def logout(request):

      return render(request,'registration/logout.html',{})

def password_change(request):

      return render(request,'registration/password_change.html',{})

def password_reset(request):

      return render(request,'registration/password_reset.html',{})


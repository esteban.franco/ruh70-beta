from django.urls import path, include
from . import views

app_name= "accounts"
# -------------CODIGO PROPIO -----------------------------

urlpatterns = [
    path('^accounts/login/$', views.login, name='login'),
    path('^accounts/logout/$', views.logout, name='logout'),
    path('^accounts/password_change/$', views.password_change, name='password_change'),
    path('^accounts/password_change/done/$', views.password_change, name='password_change_done'),
    path('^accounts/password_reset/$', views.password_reset, name='password_reset'),
    path('^accounts/password_reset/done/$', views.password_reset, name='password_reset_done'),
    path('^accounts/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.password_reset, name='password_reset_confirm'),
    path('^accounts/reset/done/$', views.password_reset, name='password_reset_complete'),
]
# -------------CODIGO PROPIO -----------------------------

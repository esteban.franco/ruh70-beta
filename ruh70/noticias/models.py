from django.db import models
from django.contrib.auth.models import User  # importando la clase Usuarios, los que se generan en el admin
                                          # para relacionarlos con el modelo post.

# Create your models here.


class Categoria(models.Model):

      nombre  = models.CharField(max_length=100)
      created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha creación')
      updated = models.DateTimeField(auto_now_add=True, verbose_name='Ultima actualización')

      class Meta:

            verbose_name='categoria'
            verbose_name_plural='categorias'

      def __str__(self):
            return self.nombre

      def gettype(self):
            tipo = type(self)

            return tipo

class Post(models.Model):

      titulo = models.CharField(verbose_name='Producto', max_length=60)
      cantidad = models.IntegerField(verbose_name='Cantidad en libras', null=True)
      precio = models.IntegerField(verbose_name='Precio / Lb')
      contenido = models.TextField()
      image=models.ImageField(upload_to='noticias', null=True, blank=True) # Null y Blank, para poder dejar los blogs sin imagenes.
      autor = models.ForeignKey(User, on_delete=models.CASCADE)
      created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha creación')
      updated = models.DateTimeField(auto_now_add=True, verbose_name='Ultima actualización')
      categorias = models.ManyToManyField(Categoria)

      class Meta:

            verbose_name = 'post'
            verbose_name_plural = 'posts'

      def __str__(self):
            return self.titulo

      def gettype(self):
            tipo = type(self)
            return tipo


class Empresa(models.Model):
      razon_social =models.TextField(verbose_name='Razon social',max_length=50)
      nit = models.IntegerField(verbose_name='NIT')


      class Meta:

            verbose_name='empresa'
            verbose_name_plural='empresas'

      def __str__(self):
            return self.razon_social
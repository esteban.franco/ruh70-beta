# Generated by Django 3.1.4 on 2021-05-31 12:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noticias', '0002_empresa'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='cantidad',
            field=models.IntegerField(null=True, verbose_name='Cantidad en libras'),
        ),
        migrations.AddField(
            model_name='post',
            name='precio',
            field=models.IntegerField(default=0, verbose_name='Precio / Lb'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='post',
            name='titulo',
            field=models.CharField(max_length=60, verbose_name='Producto'),
        ),
    ]

from django.contrib import admin

from .models import Categoria, Post, Empresa

# Register your models here.

class CategoriaAdmin(admin.ModelAdmin):

      read_only_field = ('created','updated')

class PostAdmin(admin.ModelAdmin):

      read_only_field = ('created','updated')

class empresaAdmin(admin.ModelAdmin):
      list_display=('razon_social',)
      search_fields =('nit',)

admin.site.register(Categoria, CategoriaAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Empresa, empresaAdmin)

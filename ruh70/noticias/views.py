from django.shortcuts import render
from noticias.models import Post, Categoria
# Create your views here.


def noticias(request):
      posts =Post.objects.all()
      categories = [p.categorias.all for p in posts]
      available_categ = list(dict.fromkeys(categories)) # eliminando cateogorias repetidas
      return render(request,'noticias.html',{'posts':posts, 'categ_disp':available_categ})


def categoria(request, categoria_id):

      categoria = Categoria.objects.get(id=categoria_id)
      posts = Post.objects.filter(categorias=categoria)

      return render(request, 'categorias.html',{'categoria':categoria, 'posts':posts})
from django.urls import path
from . import views

urlpatterns = [
    path('', views.noticias, name='noticias'),
    path('categoria/<int:categoria_id>/', views.categoria, name='categoria'),
]

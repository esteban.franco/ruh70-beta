"""ruh70 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from ruh70.views import inicio, home
from django.conf import settings  # para poder importar variables de settings.py
from django.conf.urls.static import static # para agregar caminos estaticos del servidor como URLs de la aplicación

urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('inicio/',inicio, name='resumen'),
    path('', home, name='home'),
    path('', include('principal.urls')),

    path('inversiones/', include('inversiones.urls')),

    path('noticias/', include('noticias.urls')),

    path('contactanos/',include('contactanos.urls')),

    path('tienda/', include('tienda.urls')),

    path('carro_de_compras/', include('carro_de_compras.urls')),

]


# changing the default link of the main website within the admin platform
admin.site.site_url = "/"

# configurando el camino hacia los urls que llevan a los archivos multimedia, fijarse la definicion
# de estas variables en settings.py
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
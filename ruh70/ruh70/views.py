#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 10:55:31 2021

@author: esteban
"""

from django.shortcuts import render
from django.http import HttpResponse


def inicio(request):
    "Vista para desplegar lo que haremos en este proyecto"
    
    return render(request, 'bienvenida.html',{})

def home(request):
    
    return render(request,'home.html',{})
    

